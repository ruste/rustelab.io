My name is Russell Bryan. I'm a programmer with a background in mathematics. My
interests range from functional programming languages, interpreters, and compilers,
 to web scraping and static site generators. Here you will find various projects that I've worked on as
well as posts on things that interest me.


##wall plotters

I built a CNC wall plotter prototype with the hope of eventually placing them in
restaurant windows. That never materialized, but it was a fun project that ended
up producing some nice plots. [Read more about it](/posts/2019-03-07-wall-plotter.html).

##laser clocks

![laser_clock](/assets/laser_clock/laser_clock_small.gif)

This is a prototype for a clock that would draw the time on a luminescent surface using a UV laser and laser galvos. I was hoping to be able to erase previous drawings and use it as a persistant display for some sort of electro-chemi-mechanical sculpture. If someone wants a way to produce standard laser galvo control signals using an Arduino or other controller I've designed all of the circuitry.

##static site generators

I've written two static site generators. The first one, [Haiku](https://gitlab.com/ruste/haiku), was written in Common lisp and uses Mustache templates to turn Markdown into static HTML. It aspired to be minimalist but powerful. The second I call [Noise](https://gitlab.com/ruste/ruste.gitlab.io). It's a Bash script that renders the site you're currently looking at. It was designed to produce simple, functional sites as quickly and effortlessly as possible using existing tools and knowledge. If you know bash, you shouldn't have to learn anything new to use it.

##programming languages

I've written several toy programming languages. The first was [RPLisp](https://gitlab.com/ruste/rplisp). It was an attempt at a clone of RPL, the language used by HP calculators. The second was [vole](https://gitlab.com/ruste/vole), a LISP-1 written in a single file of pure Java. It was designed to be embedded in an existing java application for use as an extension language. I learned a lot about the Java object system while trying to squeeze this language into one file. It was also a blast learning about implementation techniques for full tail recursion.

