#!/bin/sh

cd resume
#pdflatex resume.tex
rm resume.log
cd ..

top_bar="<div class="topbar">
          <a href="/index.html">home</a>
          <a href="/posts.html">posts</a>
          <a href="/resume/resume.pdf">resume</a>
          <a href="https://gitlab.com/ruste">git</a>
        </div>"

bottom_bar='<div class="bottombar signature"> 2019 © Russell Bryan </div>'

page_head="<head>
             <link rel="icon" type="image/png" href="/assets/ricon.png">
             <link rel="stylesheet" href="/css/main.css"> 
           </head>"

post_index="<ul class="postindex">"


for post in $(ls md); do
        post_name=$(echo $post | sed -re 's/(.+).md/\1/')
        echo "<html> 
                $page_head 
                <body> 
                  <div class="content">
                    $top_bar 
                    $(markdown md/$post) 
                    $bottom_bar
                  </div>
                </body> 
              </html>" > posts/$post_name.html
        post_index="$post_index <li> <a href=posts/$post_name.html>$post_name</a> </li>"
done;

post_index="$post_index </ul>"

posts_page=" <html> 
               $page_head
               <body>
                 $top_bar 
                 <div class="content">
                 $post_index
                 </div>
                 $bottom_bar
               </body>
             </html> "

home_page="<html>
            $page_head
            <body>
              $top_bar
              <div class="content">
              $(markdown home.md)
              </div>
              $bottom_bar
            </body>
          </html>"

echo $posts_page > posts.html
echo $home_page > index.html


